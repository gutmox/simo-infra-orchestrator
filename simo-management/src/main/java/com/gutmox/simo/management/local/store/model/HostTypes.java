package com.gutmox.simo.management.local.store.model;

import com.gutmox.simo.domain.HostType;
import com.gutmox.simo.domain.impl.HostTypeImpl;

import java.util.ArrayList;
import java.util.List;

public class HostTypes {

    private List<HostTypeImpl> hostTypes = new ArrayList<>();

    public List<HostTypeImpl> getHostTypes() {
        return hostTypes;
    }

    public void addHostType(HostType hostType) {
        hostTypes.add((HostTypeImpl) hostType);
    }

    public void removeHostType(HostType hostType) {

        hostTypes.stream().filter(type -> type.getId().equals(hostType.getId()))
                .forEach(type -> hostTypes.remove(type));
    }

    public void updateHostTypeState(HostType hostType) {

        hostTypes.stream().filter(type -> type.getId().equals(hostType.getId()))
                .forEach(type -> type.setState(hostType.getState()));
    }
}
