package com.gutmox.simo.management.local.store;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

public class AbstractLocalStorage {

    private void createFile(String fileName) throws IOException {
        File file = new File(fileName);
        file.createNewFile();
    }

    public String load(String file) throws IOException {
        if (! new File(file).exists()) {
            createFile(file);
        }

        return readDataFile(file);
    }

    private String readDataFile(String fileName) throws IOException {
        return new String(Files.readAllBytes(Paths.get(fileName)));
    }

}
