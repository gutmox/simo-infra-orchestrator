package com.gutmox.simo.management;

import com.gutmox.simo.domain.Host;
import com.gutmox.simo.domain.HostSize;
import com.gutmox.simo.domain.impl.InfrastructureFactory;
import com.gutmox.simo.management.local.store.HostTypesLocalStore;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class HostManager {

    private static HostManager instance;

    private Map<String, Host> hosts = new HashMap<>();

    public static HostManager instance() {
        if (null == instance) {
            instance = new HostManager();
        }
        return instance;
    }

    private HostManager() {
    }

    public Host createNewHost(final String type, final String desc, final HostSize size) {
        final Host host = InfrastructureFactory.createHost(type, desc, size);
        hosts.put(host.getId(), host);
        return host;
    }

    public void loadFromLocalStorage(){

        HostTypesLocalStore.instance().getHostTypes().stream()
                .filter(hostTypes -> null != hostTypes)
                .forEach(hostType ->
                    createNewHost(hostType.getType(), hostType.getDescription(), hostType.getSize()));
    }

    public List<Host> getHosts() {
        return hosts.values().stream().collect(Collectors.toList());
    }
}
