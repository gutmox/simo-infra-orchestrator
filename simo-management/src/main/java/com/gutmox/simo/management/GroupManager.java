package com.gutmox.simo.management;

import com.gutmox.simo.domain.Group;
import com.gutmox.simo.domain.GroupType;
import com.gutmox.simo.domain.Host;
import com.gutmox.simo.domain.HostSize;
import com.gutmox.simo.domain.impl.GroupTypeImpl;
import com.gutmox.simo.domain.impl.InfrastructureFactory;
import com.gutmox.simo.management.local.store.GroupTypesLocalStore;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class GroupManager {

    private static GroupManager instance;

    private GroupTypesLocalStore localStore = GroupTypesLocalStore.getInstance();

    private List<GroupType> groupTypes = new ArrayList<>();

    private Map<String, Group> groups = new HashMap<>();

    public static GroupManager instance() {
        if (null == instance) {
            instance = new GroupManager();
        }
        return instance;
    }

    private GroupManager() {
    }

    public Group createNewGroup(String type, String desc, HostSize size) {
        Group newGroup = InfrastructureFactory.createGroup(type, desc, size);
        groups.put(newGroup.getId(), newGroup);
        addNewGroupType(newGroup.getId(), type, desc, size, newGroup.getHosts().size());
        return newGroup;
    }

    public void addHostToGroup(Group group, Host host) {
        group.addHost(host);
        updateGroupTypeHostCount(group.getId(), group.getHosts().size());
    }

    public void loadFromLocalStorage() {
        getLocalStorageGroupTypeList().forEach(groupType -> {

            Group group = createNewGroup(groupType.getType(), groupType.getDescription(), groupType.getSize());
            updateServersOnGroup(group, groupType);
            if (groupType.isActiveOnBoot()) {
                group.boot();
            }
        });
    }

    private void updateServersOnGroup(Group group, GroupType groupType) {
        final int serverCount = groupType.getHostCount();
        HostManager.instance().getHosts().stream()
                .filter(host -> serverCount > 0 && host.getSize().equals(groupType.getSize()))
                .forEach(host -> addHostToGroup(group, host));
    }

    public List<GroupTypeImpl> getLocalStorageGroupTypeList() {
        return localStore.getGroupTypes();
    }

    public void addNewGroupType(String id, String type, String description, HostSize size, int hostCount) {
        GroupType groupType = InfrastructureFactory.createGroupType(id, type, description, size, hostCount);
        groupTypes.add(groupType);
    }

    public void updateGroupTypeHostCount(String id, int count) {
        groupTypes.stream().filter(groupType -> groupType.getId().equals(id))
                .forEach(groupType -> groupType.setHostCount(count));
    }

    public List<Group> getGroups() {
        return groups.values().stream().collect(Collectors.toList());
    }

}
