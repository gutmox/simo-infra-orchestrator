package com.gutmox.simo.management.local.store.model;

import com.gutmox.simo.domain.GroupType;
import com.gutmox.simo.domain.impl.GroupTypeImpl;

import java.util.ArrayList;
import java.util.List;

public class GroupTypes {
    private List<GroupTypeImpl> groupTypes = new ArrayList<>();

    public GroupTypes() {
    }

    public List<GroupTypeImpl> getGroupTypes() {
        return groupTypes;
    }

    public void addGroupType(GroupType groupType) {
        groupTypes.add((GroupTypeImpl) groupType);
    }

    public void remove(GroupType groupType) {

        groupTypes.stream().filter(type -> type.getId().equals(groupType.getId()))
                .forEach(type -> groupTypes.remove(type));
    }

    public void updateActiveStatus(GroupType groupType) {

        groupTypes.stream().filter(type -> type.getId().equals(groupType.getId()))
                .forEach(type -> type.setActiveOnBoot(groupType.isActiveOnBoot()));
    }

    public void updateHostCount(GroupType groupType) {

        groupTypes.stream().filter(type -> type.getId().equals(groupType.getId()))
                .forEach(type -> type.setHostCount(groupType.getHostCount()));
    }
}
