package com.gutmox.simo.management.local.store;

import com.google.gson.Gson;
import com.gutmox.simo.domain.impl.GroupTypeImpl;
import com.gutmox.simo.management.local.store.model.GroupTypes;

import java.io.IOException;
import java.util.List;

public class GroupTypesLocalStore extends AbstractLocalStorage{

    private static GroupTypesLocalStore instance;

    private static String STORAGE_FILE = "host_group_types.json";

    private GroupTypes groupTypes;


    public static GroupTypesLocalStore getInstance() {
        if (null == instance) {
            instance = new GroupTypesLocalStore();
        }
        return instance;
    }

    private GroupTypesLocalStore() {
        groupTypes = new GroupTypes();
        try {
            String data = load(STORAGE_FILE);
            if (null != data && data.length() > 0) {
                groupTypes = new Gson().fromJson(data, GroupTypes.class);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public List<GroupTypeImpl> getGroupTypes() {
        return groupTypes.getGroupTypes();
    }
}
