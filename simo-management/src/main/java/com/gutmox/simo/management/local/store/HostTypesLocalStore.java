package com.gutmox.simo.management.local.store;

import com.google.gson.Gson;
import com.gutmox.simo.domain.impl.HostTypeImpl;
import com.gutmox.simo.management.local.store.model.HostTypes;

import java.io.IOException;
import java.util.List;

public class HostTypesLocalStore extends AbstractLocalStorage{

    private static HostTypesLocalStore instance;

    private static String STORAGE_FILE = "host_types.json";

    private HostTypes hostTypes;


    public static HostTypesLocalStore instance() {
        if (null == instance) {
            instance = new HostTypesLocalStore();
        }
        return instance;
    }

    private HostTypesLocalStore() {
        hostTypes = new HostTypes();
        try {
            String data = load(STORAGE_FILE);
            if (null != data && data.length() > 0) {
                hostTypes = new Gson().fromJson(data, HostTypes.class);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public List<HostTypeImpl> getHostTypes() {
        return hostTypes.getHostTypes();
    }
}
