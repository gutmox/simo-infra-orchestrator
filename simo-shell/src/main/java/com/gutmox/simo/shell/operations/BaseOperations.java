package com.gutmox.simo.shell.operations;

import asg.cliche.Command;
import asg.cliche.Shell;
import asg.cliche.ShellFactory;
import com.gutmox.simo.management.GroupManager;
import com.gutmox.simo.management.HostManager;
import com.gutmox.simo.shell.operations.groups.GroupsOperations;
import com.gutmox.simo.shell.operations.helpers.HelpersMessages;
import com.gutmox.simo.shell.operations.hosts.HostsOperations;

import java.io.IOException;

public class BaseOperations {

    private String appName = "SiMo";

    private Shell shell, subShellHosts, subShellGroups;

    private Operations hostsOperations, groupsOperations;

    public BaseOperations() {

        shell = ShellFactory.createConsoleShell(">", appName, this);
        hostsOperations = new HostsOperations();
        subShellHosts = ShellFactory.createSubshell("hosts", shell, appName, hostsOperations);
        hostsOperations.setCommandShell(subShellHosts);

        groupsOperations = new GroupsOperations();
        subShellGroups = ShellFactory.createSubshell("groups", shell, appName, groupsOperations);
        groupsOperations.setCommandShell(subShellGroups);
    }

    @Command
    public String help() {
        return HelpersMessages.firstLevelHelp();
    }

    @Command
    public String load() {
        HostManager.instance().loadFromLocalStorage();
        GroupManager.instance().loadFromLocalStorage();
        return "Storage loaded";
    }

    @Command
    public void hosts() throws IOException {
        System.out.println(hostsOperations.help());
        subShellHosts.commandLoop();
    }

    @Command
    public void groups() throws IOException {
        System.out.println(groupsOperations.help());
        subShellGroups.commandLoop();
    }

    public void commandLoop() throws IOException {
        shell.commandLoop();
    }
}
