package com.gutmox.simo.shell.operations.groups;

import asg.cliche.Command;
import asg.cliche.Shell;
import asg.cliche.ShellFactory;
import com.gutmox.simo.domain.Group;
import com.gutmox.simo.management.GroupManager;
import com.gutmox.simo.shell.operations.Operations;
import com.gutmox.simo.shell.operations.helpers.HelpersMessages;

import java.io.IOException;
import java.util.List;

public class GroupsOperations implements Operations {

    private Shell shell;

    private List<Group> groups;

    public GroupsOperations() {
        groups = GroupManager.instance().getGroups();
    }

    @Override
    public void setCommandShell(Shell cmdShell) {
        shell = cmdShell;
    }

    @Command
    public String help() {
        return HelpersMessages.operationsHelp("Group");
    }

    @Command
    public String create() {
        Group group = GroupCreation.createGroup();
        groups = GroupManager.instance().getGroups();
        return group.toString();
    }

    @Command
    public String list() {
        groups = GroupManager.instance().getGroups();

        if (groups.isEmpty()) {
            return "There are no groups yet";
        }

        String groupList = "";
        for (int i = 0; i < groups.size(); i++) {
            groupList += i + ": " + groups.get(i).toString() + "\n";
        }
        return groupList;
    }

    @Command
    public String select(int which) {
        if (which < 0 || null == groups || which >= groups.size()) {
            return "Please select correct group";
        }
        Group selectedGroup = groups.get(which);
        openGroupsSubShell(selectedGroup);
        return "Group " + selectedGroup.getId();
    }

    private void openGroupsSubShell(Group group){
        GroupsInstancesOperations groupsInstancesOperations = new GroupsInstancesOperations(group, shell);
        Shell hostGroupCmdShell = ShellFactory.createSubshell(group.getId(), shell, "", groupsInstancesOperations);

        System.out.println(groupsInstancesOperations.help());

        try {
            hostGroupCmdShell.commandLoop();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void exit() {
        try {
            groups = GroupManager.instance().getGroups();
            System.out.println(help());
            shell.commandLoop();
        } catch (IOException ex) {

        }
    }
}
