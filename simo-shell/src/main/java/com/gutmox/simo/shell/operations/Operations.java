package com.gutmox.simo.shell.operations;

import asg.cliche.Shell;

public interface Operations {

    String help();

    String create();

    String list();

    String select(int instance);

    void exit();

    void setCommandShell(Shell subShellHosts);
}
