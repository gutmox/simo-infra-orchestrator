package com.gutmox.simo.shell.operations.hosts;

import asg.cliche.Command;
import asg.cliche.Shell;
import asg.cliche.ShellFactory;
import com.gutmox.simo.domain.Host;
import com.gutmox.simo.management.HostManager;
import com.gutmox.simo.shell.operations.Operations;
import com.gutmox.simo.shell.operations.helpers.HelpersMessages;

import java.io.IOException;
import java.util.List;

public class HostsOperations implements Operations {

    private Shell shell;

    private List<Host> hosts;

    public HostsOperations() {
        hosts = HostManager.instance().getHosts();
    }

    @Override
    public void setCommandShell(Shell cmdShell) {
        shell = cmdShell;
    }

    @Command
    public String help() {
        return HelpersMessages.operationsHelp("Host");
    }

    @Command
    public String create() {
        HostCreation.createHost();
        return "Creating Host";
    }

    @Command
    public String list() {

        hosts = HostManager.instance().getHosts();
        if (hosts.isEmpty()) {
            return "There are no Host yet.";
        }

        String hostList = "";
        for (int i = 0; i < hosts.size(); i ++) {
            hostList += i + ": " + hosts.get(i).toString() + "\n";
        }
        return hostList;
    }

    @Command
    public String select(int instance) {
        if (instance < 0 || null == hosts || instance >= hosts.size()) {
            return "Invalid instance";
        }
        Host selectedHost = hosts.get(instance);
        opeHostInstanceSubShell(selectedHost);
        return "Host " + selectedHost.getId();
    }

    private void opeHostInstanceSubShell(Host selectedHost) {
        try {
            HostsInstancesOperations hostCommands = new HostsInstancesOperations(selectedHost, shell);
            Shell hostCmdShell = ShellFactory.createSubshell(selectedHost.getId(), shell, "", hostCommands);

            System.out.println(hostCommands.help());

            hostCmdShell.commandLoop();
        } catch (IOException ex) {

        }
    }

    @Override
    public void exit() {
        try {
            hosts = HostManager.instance().getHosts();
            System.out.println(help());
            shell.commandLoop();
        } catch (IOException ex) {

        }
    }
}
