package com.gutmox.simo.shell.operations.hosts;

import asg.cliche.Command;
import asg.cliche.Shell;
import com.gutmox.simo.domain.Host;
import com.gutmox.simo.shell.operations.OperationsOnInstances;
import com.gutmox.simo.shell.operations.helpers.HelpersMessages;

import java.io.IOException;

public class HostsInstancesOperations implements OperationsOnInstances {
    private final Shell shell;
    private Host host;

    public HostsInstancesOperations(Host instance, Shell shell) {
        host = instance;
        this.shell = shell;

    }

    @Command
    public String help() {
        return HelpersMessages.hostsInstancesHelp();
    }

    @Command
    public String boot() {
        try {
            host.boot();
            return "Booting host";
        } catch (IllegalStateException ex) {
            return "Operation not allowed in current state";
        }
    }

    @Command
    public String activate() {
        try {
            host.activate();
            return "Activating host";
        } catch (IllegalStateException ex) {
            return "Operation not allowed in current state";
        }
    }

    @Command
    public String deactivate() {
        try {
            host.deactivate();
            return "Deactivating host";
        } catch (IllegalStateException ex) {
            return "Operation not allowed in current state";
        }
    }


    @Command
    public String shutdown() {
        try {
            host.shutdown();
            return "Shutting down host";
        } catch (IllegalStateException ex) {
            return "Operation not allowed in current state";
        }
    }

    @Command
    public void exit() throws IOException {
        System.out.println(help());
        shell.commandLoop();
    }

    @Command
    public String status() {
        return host.toString();
    }
}
