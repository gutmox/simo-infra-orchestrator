package com.gutmox.simo.shell;

import com.gutmox.simo.shell.operations.BaseOperations;

import java.io.IOException;

public class Runner {

    public static void main(String[] args) throws IOException {

        BaseOperations baseOperations = new BaseOperations();

        System.out.println(baseOperations.help());

        baseOperations.commandLoop();
    }
}
