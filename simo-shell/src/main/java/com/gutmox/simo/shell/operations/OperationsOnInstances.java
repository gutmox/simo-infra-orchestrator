package com.gutmox.simo.shell.operations;

import java.io.IOException;

public interface OperationsOnInstances {

    String help();

    String boot();

    String shutdown();

    void exit() throws IOException;
}
