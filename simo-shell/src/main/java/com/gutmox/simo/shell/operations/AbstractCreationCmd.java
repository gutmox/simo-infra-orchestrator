package com.gutmox.simo.shell.operations;

import com.gutmox.simo.domain.HostSize;
import com.gutmox.simo.domain.ServiceType;

import java.util.InputMismatchException;
import java.util.Scanner;

public abstract class AbstractCreationCmd {

    public static String chooseService() {
        StringBuilder typeBuilder = new StringBuilder("\n");
        ServiceType[] serviceTypes = ServiceType.values();
        for (int i = 0; i < serviceTypes.length; i++) {
            typeBuilder.append(" " + i + ": " + serviceTypes[i].name() + "\n");
        }
        typeBuilder.append("Select service type: \n");
        System.out.println(typeBuilder.toString());

        do {
            try {
                Scanner outputScanner = new Scanner(System.in);
                int selection = outputScanner.nextInt();
                if (selection >= 0 && selection < serviceTypes.length) {
                    return serviceTypes[selection].name();
                } else {
                    System.out.println("Incorrect value");
                }
            } catch (InputMismatchException ex) {
                System.out.println("Incorrect value");
            }
        } while (true);
    }

    public static String enterDescription() {
        System.out.print("Enter the description: ");
        Scanner outputScanner = new Scanner(System.in);
        String description = outputScanner.nextLine();
        return description;
    }

    public static HostSize chooseSize() {
        StringBuilder typeBuilder = new StringBuilder("\n");
        typeBuilder.append(" 0 : " + HostSize.SMALL + "\n");
        typeBuilder.append(" 1 : " + HostSize.MEDIUM + "\n");
        typeBuilder.append(" 2 : " + HostSize.LARGE + "\n");
        typeBuilder.append("Choose instance size:");
        System.out.println(typeBuilder.toString());
        do {
            try {
                Scanner outputScanner = new Scanner(System.in);
                int selection = outputScanner.nextInt();
                if (selection >= 0 && selection < 3) {
                    HostSize size = HostSize.SMALL;
                    switch (selection) {
                        case 0:
                            size = HostSize.SMALL;
                            break;
                        case 1:
                            size = HostSize.MEDIUM;
                            break;
                        case 2:
                            size = HostSize.LARGE;
                            break;
                    }
                    return size;
                } else {
                    System.out.println("Selection is invalid");
                }
            } catch (InputMismatchException ex) {
                System.out.println("Selection is invalid");
            }
        } while (true);
    }
}
