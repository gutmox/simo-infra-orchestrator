package com.gutmox.simo.shell.operations.helpers;

public class HelpersMessages {

    public static String firstLevelHelp() {
        String helpMessage = "Operations:                        \n";
        helpMessage += "help\n";
        helpMessage += "load: Load from file store \n";
        helpMessage += "hosts: Hosts Operations    \n";
        helpMessage += "groups: Group Operations    \n";
        helpMessage += "exit\n";

        return helpMessage;
    }

    private static String instancesHelp(String type) {
        String helpMessage = "Operations:                    \n";
        helpMessage += "help\n";
        helpMessage += "boot: boot " + type + "         \n";
        helpMessage += "activate:  indicates if it should be brought into the load balancer \n";
        helpMessage += "deactivate: deactivate " + type + "     \n";
        helpMessage += "shutdown: shutdown " + type + "       \n";
        helpMessage += "status: get " + type + " status          \n";
        return helpMessage;
    }

    public static String groupInstancesHelp() {
        String helpMessage = instancesHelp("Group");
        helpMessage += "dependencies: host group types (IDs) this group type depends on\n";
        helpMessage += "hosts: hosts which belong to this group";
        helpMessage += "exit: Exit Group \n";

        return helpMessage;
    }

    public static String hostsInstancesHelp() {
        String helpMessage = instancesHelp("Host");
        helpMessage += "exit: Exit Host \n";
        return helpMessage;
    }

    public static String operationsHelp(String type) {
        String helpMessage = "Operations:                    \n";
        helpMessage += "help\n";
        helpMessage += "create: create " + type + " \n";
        helpMessage += "list: list " + type + "s \n";
        helpMessage += "select: select " + type + " \n";
        helpMessage += "exit\n";

        return helpMessage;
    }
}
