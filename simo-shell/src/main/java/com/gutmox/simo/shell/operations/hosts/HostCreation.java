package com.gutmox.simo.shell.operations.hosts;

import com.gutmox.simo.domain.Group;
import com.gutmox.simo.domain.Host;
import com.gutmox.simo.domain.HostSize;
import com.gutmox.simo.management.GroupManager;
import com.gutmox.simo.management.HostManager;
import com.gutmox.simo.shell.operations.AbstractCreationCmd;

import java.util.InputMismatchException;
import java.util.List;
import java.util.Scanner;

public class HostCreation extends AbstractCreationCmd {

    private HostCreation() {
    }

    static void createHost() {
        String hostType = chooseService();
        String description = enterDescription();
        HostSize size = chooseSize();
        final Group group = chooseGroup();
        Host newHost = HostManager.instance().createNewHost(hostType, description, size);
        GroupManager.instance().addHostToGroup(group, newHost);
    }

    private static Group chooseGroup() {
        StringBuilder builder = new StringBuilder();
        builder.append("Append this instance to a group\n");
        List<Group> groupList = GroupManager.instance().getGroups();
        if (! groupList.isEmpty()) {
            for (int i = 0; i < groupList.size(); i++) {
                builder.append(" " + i + ": " + groupList.get(i).toString() + "\n");
            }
        }
        System.out.println(builder.toString());
        do {
            try {
                Scanner outputScanner = new Scanner(System.in);
                int selection = outputScanner.nextInt();
                if (selection >= 0 && selection < groupList.size()) {
                    Group group = groupList.get(selection);
                    return group;
                } else {
                    System.out.println("Selection is invalid");
                }
            } catch (InputMismatchException ex) {
                System.out.println("Selection is invalid");
            }
        } while (true);
    }
}
