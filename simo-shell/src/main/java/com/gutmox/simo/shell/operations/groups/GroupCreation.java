package com.gutmox.simo.shell.operations.groups;

import com.gutmox.simo.domain.Group;
import com.gutmox.simo.domain.HostSize;
import com.gutmox.simo.management.GroupManager;
import com.gutmox.simo.shell.operations.AbstractCreationCmd;

class GroupCreation extends AbstractCreationCmd {

    static Group createGroup() {
        String groupType = chooseService();
        String description = enterDescription();
        HostSize size = chooseSize();
        return GroupManager.instance().createNewGroup(groupType, description, size);
    }
}
