package com.gutmox.simo.shell.operations.groups;

import asg.cliche.Command;
import asg.cliche.Shell;
import com.gutmox.simo.domain.Group;
import com.gutmox.simo.shell.operations.OperationsOnInstances;
import com.gutmox.simo.shell.operations.helpers.HelpersMessages;

import java.io.IOException;

public class GroupsInstancesOperations implements OperationsOnInstances {
    private final Shell shell;
    private Group group;

    public GroupsInstancesOperations(Group group, Shell shell) {
        this.group = group;
        this.shell = shell;
    }

    @Command
    public String help() {
        return HelpersMessages.groupInstancesHelp();
    }

    @Command
    public String boot() {
        try {
            group.boot();
            return "Booting Host";
        } catch (IllegalStateException ex) {
            return "Operation not allowed in current state";
        }
    }

    @Command
    public String activate() {
        try {
            group.activate();
            return "Activating group";
        } catch (IllegalStateException ex) {
            return "Operation not allowed in current state";
        }
    }

    @Command
    public String deactivate() {
        try {
            group.deactivate();
            return "Deactivating group";
        } catch (IllegalStateException ex) {
            return "Operation not allowed in current state";
        }
    }


    @Command
    public String shutdown() {
        try {
            group.shutdown();
            return "Shutdown Host";
        } catch (IllegalStateException ex) {
            return "Operation not allowed in current state";
        }
    }

    @Command
    public String dependencies() {

        if (group.getHosts().isEmpty()) {
            return "There are no Host yet.";
        }

        String hostList = "";
        for (int i = 0; i < group.getHosts().size(); i ++) {
            hostList += group.getHosts().get(i).getId() + "\n";
        }
        return hostList;
    }

    @Command
    public String hosts() {

        if (group.getHosts().isEmpty()) {
            return "There are no Host yet.";
        }

        String hostList = "";
        for (int i = 0; i < group.getHosts().size(); i ++) {
            hostList += group.getHosts().get(i).toString() + "\n";
        }
        return hostList;
    }

    @Command
    public String status() {
        return group.toString();
    }

    @Command
    public void exit() throws IOException {
        System.out.println(help());
        shell.commandLoop();
    }
}
