package com.gutmox.simo.domain.impl;

import com.gutmox.simo.domain.Host;
import com.gutmox.simo.domain.HostSize;
import com.gutmox.simo.domain.HostStatus;
import com.gutmox.simo.fsm.Events;
import com.gutmox.simo.fsm.FSM;

import java.util.UUID;

public class HostImpl implements Host {

    private FSM fsm = new FSM("Hosts");

    private String id = UUID.randomUUID().toString();
    private String type;
    private String description;
    private HostSize hostSize;
    private boolean active = false;

    HostImpl(String type, String description, HostSize hostSize) {
        this.type = type;
        this.description = description;
        this.hostSize = hostSize;
        initStates();
        initTransitions();
    }

    @Override
    public void boot(){

        if (! getState().equals(HostStatus.CREATED)) {
            throw new IllegalStateException();
        }
        fsm.addEvent(Events.START.name());
    }

    @Override
    public void shutdown(){

        if (! getState().equals(HostStatus.RUNNING)) {
            throw new IllegalStateException();
        }

        fsm.addEvent(Events.SHUTDOWN.name());
    }

    @Override
    public void activate() {

        if (active) {
            throw new IllegalStateException();
        }
        active = true;

    }

    @Override
    public void deactivate() {
        if (! active) {
            throw new IllegalStateException();
        }
        active = false;
    }

    private void initStates() {
        fsm.addState(HostStatus.CREATED.name());
        fsm.addState(HostStatus.BOOTING.name());
        fsm.addState(HostStatus.RUNNING.name());
        fsm.addState(HostStatus.SHUTTING_DOWN.name());
        fsm.addState(HostStatus.SHUTDOWN.name());
        fsm.setAutoTransition(HostStatus.BOOTING.name(), HostStatus.RUNNING.name());
        fsm.setAutoTransition(HostStatus.SHUTTING_DOWN.name(), HostStatus.SHUTDOWN.name());
    }

    private void initTransitions() {

        fsm.addTransition(new FSM.Transition(Events.START.name(), HostStatus.CREATED.name(), HostStatus.BOOTING.name()));
        fsm.addTransition(new FSM.Transition(Events.SHUTDOWN.name(), HostStatus.RUNNING.name(), HostStatus.SHUTTING_DOWN.name()));
    }

    @Override
    public String getId() {
        return id;
    }

    @Override
    public String getType() {
        return type;
    }

    @Override
    public String getDescription() {
        return description;
    }

    @Override
    public HostSize getSize() {
        return hostSize;
    }

    @Override
    public HostStatus getState() {
        return HostStatus.valueOf(fsm.getState());
    }

    @Override
    public boolean isActive() {
        return active;
    }

    @Override
    public String toString() {
        return "{" +
                "id='" + id + '\'' +
                ", hostState=" + getState() +
                ", type='" + type + '\'' +
                ", active='" + active + '\'' +
                ", description='" + description + '\'' +
                ", hostSize=" + hostSize +
                '}';
    }
}
