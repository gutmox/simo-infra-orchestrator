package com.gutmox.simo.domain;

public enum GroupState {
    CREATED,
    BOOTING,
    RUNNING,
    SHUTTING_DOWN,
    SHUTDOWN
}
