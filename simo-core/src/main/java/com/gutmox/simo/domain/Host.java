package com.gutmox.simo.domain;

public interface Host {

    String getId ();

    String getType ();

    String getDescription ();

    HostSize getSize ();

    HostStatus getState ();

    boolean isActive ();

    void boot ();

    void shutdown ();

    void activate();

    void deactivate();
}
