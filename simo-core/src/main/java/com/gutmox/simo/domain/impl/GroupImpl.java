package com.gutmox.simo.domain.impl;

import com.gutmox.simo.domain.Group;
import com.gutmox.simo.domain.GroupState;
import com.gutmox.simo.domain.Host;
import com.gutmox.simo.domain.HostSize;
import com.gutmox.simo.fsm.Events;
import com.gutmox.simo.fsm.FSM;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class GroupImpl implements Group {

    private FSM fsm = new FSM("Groups");

    private String id = UUID.randomUUID().toString();
    private String type;
    private String description;
    private HostSize hostSize;
    private List<Host> hosts = new ArrayList<>();
    private boolean active = false;

    public GroupImpl(String type, String description, HostSize hostSize) {
        this.type = type;
        this.description = description;
        this.hostSize = hostSize;
        initStates();
        initTransitions();
    }

    @Override
    public String getId() {
        return id;
    }

    @Override
    public String getType() {
        return type;
    }

    @Override
    public String getDescription() {
        return description;
    }

    @Override
    public HostSize getSize() {
        return hostSize;
    }

    @Override
    public GroupState getState() {
        return GroupState.valueOf(fsm.getState());
    }

    @Override
    public void addHost(Host host) {
        hosts.add(host);
    }

    @Override
    public List<Host> getHosts() {
        return hosts;
    }

    @Override
    public void boot() {

        if (!getState().equals(GroupState.CREATED)) {
            throw new IllegalStateException();
        }
        fsm.addEvent(Events.START.name());
    }

    @Override
    public void shutdown() {

        if (!getState().equals(GroupState.RUNNING)) {
            throw new IllegalStateException();
        }
        fsm.addEvent(Events.SHUTDOWN.name());
    }

    @Override
    public void activate() {
        if (active) {
            throw new IllegalStateException();
        }
        active = true;
    }

    @Override
    public void deactivate() {
        if (! active) {
            throw new IllegalStateException();
        }
        active = false;

    }

    private void initStates() {
        fsm.addState(GroupState.CREATED.name());
        fsm.addState(GroupState.BOOTING.name());
        fsm.addState(GroupState.RUNNING.name());
        fsm.addState(GroupState.SHUTTING_DOWN.name());
        fsm.addState(GroupState.SHUTDOWN.name());

        fsm.setAutoTransition(GroupState.BOOTING.name(), GroupState.RUNNING.name());
        fsm.setAutoTransition(GroupState.SHUTTING_DOWN.name(), GroupState.SHUTDOWN.name());
    }

    private void initTransitions() {
        fsm.addTransition(new FSM.Transition(Events.START.name(), GroupState.CREATED.name(), GroupState.BOOTING.name()));
        fsm.addTransition(new FSM.Transition(Events.SHUTDOWN.name(), GroupState.RUNNING.name(), GroupState.SHUTTING_DOWN.name()));
    }
    @Override
    public String toString() {
        return "{" +
                "id='" + id + '\'' +
                ", type='" + type + '\'' +
                ", description='" + description + '\'' +
                ", hostSize=" + hostSize +
                ", groupState=" + getState() +
                ", active=" + active +
                '}';
    }
}
