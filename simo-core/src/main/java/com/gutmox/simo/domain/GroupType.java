package com.gutmox.simo.domain;

public interface GroupType extends HostType {

    HostSize getSize ();

    int getHostCount ();

    void setHostCount(int count);
    
    boolean isActiveOnBoot ();

    void setActiveOnBoot(boolean active);
}
