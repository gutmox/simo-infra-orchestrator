package com.gutmox.simo.domain;

import java.util.List;

public interface Group {

    String getId ();

    String getType ();

    String getDescription ();

    HostSize getSize ();

    GroupState getState();

    public void addHost(Host host);
    
    List<Host> getHosts ();

    void boot();

    void shutdown();

    void activate();

    void deactivate();
}
