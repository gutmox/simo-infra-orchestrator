package com.gutmox.simo.domain;

public enum HostSize {
    SMALL,
    MEDIUM,
    LARGE
}
