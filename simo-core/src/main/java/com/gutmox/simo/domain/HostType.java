package com.gutmox.simo.domain;

import java.lang.String;

public interface HostType {

    String getId ();

    String getType ();

    String getDescription ();
    
    HostSize getSize ();
    
    HostStatus getState();
    
    void setState(HostStatus state);
}
