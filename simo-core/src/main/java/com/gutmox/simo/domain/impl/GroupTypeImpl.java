package com.gutmox.simo.domain.impl;

import com.gutmox.simo.domain.GroupType;
import com.gutmox.simo.domain.HostSize;

public class GroupTypeImpl extends HostTypeImpl implements GroupType {

	private int hostCount;
	private boolean isActive;


    public GroupTypeImpl() {
        super();
    }

    public GroupTypeImpl(String id, String type, String description, HostSize hostSize, int hostCount) {
		super(id, type, description, hostSize);
		this.hostCount = hostCount;
	}

	@Override
    public int getHostCount() {
        return hostCount;
    }

	@Override
    public void setHostCount(int count){
    	hostCount = count;
    }
    
    @Override
    public boolean isActiveOnBoot() {
        return isActive;
    }

    @Override
    public void setActiveOnBoot(boolean active){
    	isActive = active;
    }
    
}
