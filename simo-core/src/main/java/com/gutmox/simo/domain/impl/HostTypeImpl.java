package com.gutmox.simo.domain.impl;

import com.gutmox.simo.domain.HostSize;
import com.gutmox.simo.domain.HostType;
import com.gutmox.simo.domain.HostStatus;

public class HostTypeImpl implements HostType {

	private String id;
	private String type;
	private String description;
	private HostSize size;
	private HostStatus mHostState;
	
	public HostTypeImpl(){
		
	}
	
	public HostTypeImpl(String id, String type, String description, HostSize size){
        this.id = id;
        this.type = type;
        this.description = description;
        this.size = size;
	}

    @Override
    public String getId() {
        return id;
    }

    @Override
    public String getType() {
        return type;
    }

    @Override
    public String getDescription() {
        return description;
    }

    @Override
    public HostSize getSize() {
        return size;
    }

    @Override
    public HostStatus getState(){
    	if(null == mHostState){
    		mHostState = HostStatus.CREATED;
    	}
    	return mHostState;
    }
    
    @Override
    public void setState(HostStatus state){
    	mHostState = state;
    }


}

