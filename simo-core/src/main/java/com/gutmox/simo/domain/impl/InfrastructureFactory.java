package com.gutmox.simo.domain.impl;

import com.gutmox.simo.domain.Group;
import com.gutmox.simo.domain.GroupType;
import com.gutmox.simo.domain.Host;
import com.gutmox.simo.domain.HostSize;
import com.gutmox.simo.domain.HostType;

public class InfrastructureFactory {

    public static Host createHost(String type, String description, HostSize hostSize) {

        return new HostImpl(type, description, hostSize);
    }

    public static HostType createHostType(String id, String type, String description, HostSize hostSize) {

        return new HostTypeImpl(id, type, description, hostSize);
    }

    public static Group createGroup(String type, String description, HostSize hostSize) {

        return new GroupImpl(type, description, hostSize);
    }

    public static GroupType createGroupType(String id, String type, String description, HostSize hostSize, int hostCount) {

        return new GroupTypeImpl(id, type, description, hostSize, hostCount);
    }
}
