package com.gutmox.simo.domain;

public enum HostStatus {
    CREATED,
    BOOTING,
    RUNNING,
    SHUTTING_DOWN,
    SHUTDOWN
}